
var $j = jQuery.noConflict();

$j(document).ready(function() {
	"use strict";

	$j('.single-product .q_accordion_holder .title-holder.description_tab').removeClass('ui-state-default ui-corner-top ui-corner-bottom').addClass('ui-corner-top ui-accordion-header-active ui-state-active');

$j('.single-product .q_accordion_holder .title-holder.description_tab + .accordion_content').css('display', 'block');
});
