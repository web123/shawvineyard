<?php
/**
 * Template Name: Events
 *
 */
get_header(); ?>

<style>
	.i001-event-calendar{float:right;margin-bottom:11px;padding-left:7px;width:237px}.i001-calendar-holder0{background:#eff9f7;border-radius:3px;padding:3px}.i001-calendar-header0{background:#83d3bd;border-radius:5px;height:31px;padding:3px}.i001-event-calendar .i001-calendar-hdr-left{float:left}.i001-event-calendar .i001-calendar-hdr-left a.arrow span{border-left:medium none;border-right-color:#fff}.i001-event-calendar .i001-calendar-hdr-right{float:right}.i001-calendar-header0 a{display:inline-block;padding-top:4px}.i001-calendar-header0 a.arrow{background:#73cdb4;border-radius:4px;display:inline-block;height:31px;padding:0;position:relative;vertical-align:top;width:31px}.i001-calendar-header0 a:active,.i001-calendar-header0 a:link,.i001-calendar-header0 a:visited{color:#fff;font-size:14pt;text-decoration:none}.i001-calendar-header0 a.arrow span{-moz-border-bottom-colors:none;-moz-border-left-colors:none;-moz-border-right-colors:none;-moz-border-top-colors:none;border-color:transparent transparent transparent #fff;border-image:none;border-style:solid;border-width:6px;height:0;left:13px;position:absolute;top:10px;width:0}table.i001-calendar{border-collapse:collapse;margin:7px auto;width:221px}.i001-event-list .i001-list{border-right:1px dashed #e5e5e5;margin-right:244px}.i001-list{margin-bottom:11px}.i001-event-list .i001-list-item{border-top:1px dashed #e5e5e5;overflow:hidden;padding:16px 0 18px}.i001-event-list .i001-list-item .i001-list-image{float:left;width:156px}.i001-event-list .i001-list-item .i001-list-wrap{margin:0 0 0 156px;padding:0 0 0 26px}.i001-event-list .i001-list-item .i001-list-wrap { float: none;}
	table.main td { vertical-align: top; font-family: verdana,arial, helvetica,  sans-serif; font-size: 11px; } table.main th { border-width: 1px 1px 1px 1px; padding: 0px 0px 0px 0px; } table td {
  padding-left: 2px;
  padding-right: 0.5px;
}table.main {
  background-color: #EFF9F7;
  width: 216px;
}
table.main a{TEXT-DECORATION: none;}
</style>

<?php
if($_GET['date']){
	$date= $_GET['date'];
	if (preg_match("/^[0-9]{4}-([1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        $fetches_date =explode("-", $date);
		$current_year = $fetches_date['0'];;
        $current_month = $fetches_date['1'];;
        $current_date = $fetches_date['2'];
    }else{
        $current_year = get_the_date("Y");
		$current_month = get_the_date("n");
		$current_date = get_the_date("j");
    }
}
else{
	$current_year = get_the_date("Y");
	$current_month = get_the_date("n");
	$current_date = get_the_date("j");
}


$previous_month = $current_month - 1;
$previous_year = $current_year - 1;
$next_month = $current_month + 1;
$next_year = $current_month + 1;
//if(strlen ( $next_month)==1){$next_month='0'.$next_month; }
//echo $next_month;
?>
<div class="container">
<div class="container_inner">

<div class="sidebar-area wpb_column vc_column_container vc_col-sm-4">
	<?php if ( is_active_sidebar( 'secondary-sidebar-2' ) ) : ?>
	<div id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'secondary-sidebar-2' ); ?>
	</div>
<?php endif; ?>

</div><!--vc_col-sm-4-->



  <div class="wpb_column vc_column_container vc_col-sm-8">
	<div class="custom__blogs_h">
		<div class="i001-event-list">
			<div class="i001-event-calendar">
				<div class="i001-calendar-holder00">
					<div class="i001-calendar-header0">
						<div class="i001-calendar-hdr-left">
							<a class="arrow" href="<?php echo get_permalink()."?date=$current_year-$previous_month-01"; ?>"><span></span></a> &nbsp;
							<a href="/events.aspx?lv.date=2016-12-01">Previous</a>
						</div><!--i001-calendar-hdr-left-->

						<div class="i001-calendar-hdr-right">
							<a href="/events.aspx?lv.date=2017-02-01">Next</a> &nbsp;
							<a class="arrow" href="<?php echo get_permalink()."?date=$current_year-$next_month-01" ?>"><span></span></a>
						</div><!--i001-calendar-hdr-right-->
					</div><!--i001-calendar-header0-->

					<div class="i001-calendar-month">
<?Php

if($_GET['date']){
	$date= $_GET['date'];
	if (preg_match("/^[0-9]{4}-([1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        $fetch_date =explode("-", $date);
		$cy = $fetch_date['0'];
		$cm = $fetch_date['1'];
    }else{
        $cy= $current_year;
    }
}
else{
	$cy= $current_year;
	$cm = $current_month;
}


$year=$cy; // change this to another year
$row=0; // to set the number of rows and columns in yearly calendar
echo "<table class='main'>"; // Outer table
////// Starting of for loop///
///  Creating calendars for each month by looping 12 times ///
for($m=$cm;$m<=$cm+2;$m++){
$month =date($m);  // Month
$dateObject = DateTime::createFromFormat('!m', $m);
$monthName = $dateObject->format('F'); // Month name to display at top
$d= 2; // To Finds today's date
//$no_of_days = date('t',mktime(0,0,0,$month,1,$year)); //This is to calculate number of days in a month
$no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);//calculate number of days in a month

$j= date('w',mktime(0,0,0,$month,1,$year)); // This will calculate the week day of the first day of the month
//echo $j;// Sunday=0 , Saturday =6
//// if starting day of the week is Monday then add following two lines ///
$j=$j-1;
if($j<0){$j=6;}  // if it is Sunday //
//// end of if starting day of the week is Monday ////


$adj=str_repeat("<td bgcolor='#ffff00'> </td>",$j);  // Blank starting cells of the calendar
$blank_at_end=42-$j-$no_of_days; // Days left after the last day of the month
if($blank_at_end >= 7){$blank_at_end = $blank_at_end - 7 ;}
$adj2=str_repeat("<td bgcolor='#ffff00'> </td>",$blank_at_end); // Blank ending cells of the calendar

/// Starting of top line showing year and month to select ///////////////
if(($row % 1)== 0){
echo "</tr><tr>";
}

echo "<td><table class='main' ><td colspan=6 align=center style='text-align: left; font-family: arial; font-weight: bold; color: rgb(100, 200, 172); font-size: 13px;'> $monthName $year


 </td><td align='center'></td></tr>";
//echo "<tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr><tr>";
echo "<tr><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th><th>Su</th></tr><tr>";
////// End of the top line showing name of the days of the week//////////

//////// Starting of the days//////////
for($i=1;$i<=$no_of_days;$i++){

/* For Date */
	if($_GET['date']){
				$date= $_GET['date'];
				if (preg_match("/^[0-9]{4}-([1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    			{
    				$today = $_GET['date'];
  					$time = strtotime($_GET['date']);
  					$newformat = date('Ymd',$time);
    			}
    			else{
    			$newformat1 = date('Ym')."01";

   				 }
	}	else{
	$newformat1 = date('Ym')."01";
    }
 /* End Date */
$args = array(
    'post_type' => 'event',
    'posts_per_page' => '-1',
    'meta_key' => 'date',
    'meta_query' => array(
       array(
            'key' => 'date',
            'value' => $newformat1,
            'compare' => '>='
        )
    ),
    'orderby' => 'meta_value_num',
    'order' => 'ASC'
);

$mypost_one = new WP_Query($args);
            global $post;
            $posts = $mypost_one->get_posts();

//print_r($posts);
            $flag=0;
 foreach ($posts as $post) {
//echo "<pre>"; print_r($post);
           $start_time = get_field('start_time');
			$end_time = get_field('end_time');
			$MATCHES_DATE = get_field('date');


$pv= $year."0".$month."".$i;

if($pv == $MATCHES_DATE){

echo $adj."<td class='events_opens'>
<a href='#'  style='background: #e0f4ee; border:2px solid #eff9f7; color:#5a5a5a; font-size:13px; font-family:arial; padding:6px; text-align:center; background:#89d5c0;color:#fff'>$i</a>";
// This will display the date inside the calendar cell
?>
<div class="events">
<div class="events-hdr" >
	<h5 style="color: rgb(255, 255, 255) !important; font-size: 14px !important;">
 <?php echo date('l F j Y', strtotime(get_field('date')));  ?>
 </h5>
</div><!--events-hdr-->


<div class="event-img"><img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt=""></div>

<div class="event-item">


							<h5 style="text-transform: lowercase; font-family: arial;    margin-left: 12px; font-weight: bold; font-size: 15px;"> <?php if (strpos($end_time, 'pm') !== false) { echo $start_time." till ".$end_time;}
								  	else{ echo $start_time." - ".$end_time; }
								  ?>
								 </h5>
								 <h4><a href="<?php echo get_post_permalink($post->ID); ?>" style="padding-top: 0px; font-weight: 900; display: block; padding-bottom: 20px; font-family: arial; font-size: 16px;"><?php echo $post->post_title ?></a></h4>

<p style="color: #222;
    line-height: 14px;
    font-size: 12px;
font-weight: 300;padding:10px;"><?php
								$content = $post->post_content;
$content = preg_replace("/<img[^>]+\>/i", " ", $content);
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo wp_trim_words( $content, 40, '...' ); ?>

  <?php echo '<a type="button"  href="' . get_post_permalink($post->ID) . '" class="i001-css-button new_v01">Read More</a>'; ?></p>

	</div> <!--event-item-->
	</div><!--events-->
	</div> <!--i001-calendar-month-->
<?php
 echo " </td>";
$flag=1;
 }
}
if($flag!=1){echo $adj."<td><a href='#' onclick=\"post_value($pv);\" style='background:#e0f4ee; border:2px solid #eff9f7; color:#5a5a5a; font-size:13px; font-family:arial; padding:6px; text-align:center'>$i</a>"; // This will display the date inside the calendar cell
echo " </td>";	}

$adj='';
$j ++;

	if($j==7){
		echo "</tr><tr>"; // start a new row
		$j=0;
	}



}
echo $adj2;   // Blank the balance cell of calendar at the end

echo "</tr></table></td>";

$row=$row+1;
} // end of for loop for 12 months
echo "</table>";
?>
</div><!--i001-calendar-header00-->

	</div><!--i001-event-calendar-->
			</div><!--i001-event-list-->

			<?php
 /*
 * THis is query for all events matching the date
 */
  if($_GET['date']){
	$date= $_GET['date'];
	if (preg_match("/^[0-9]{4}-([1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
    	$today = $_GET['date'];
  		$time = strtotime($_GET['date']);
  		$newformat = date('Ymd',$time);
    }
    else{
    	$newformat = date('Ym')."01";
    	echo $newformat;
    }
}else{
	$newformat = date('Ym')."01";
    }
$args = array(
    'post_type' => 'event',
    'posts_per_page' => '-1',
    'meta_key' => 'date',
    'meta_query' => array(
       array(
            'key' => 'date',
            'value' => $newformat,
            'compare' => '>='
        )
    ),
    'orderby' => 'meta_value_num',
    'order' => 'ASC'
);

$mypost = new WP_Query($args);
            global $post;
            $posts = $mypost->get_posts();
            foreach ($posts as $post) {
//echo "<pre>"; print_r($post);
            		$start_time = get_field('start_time');
					$end_time = get_field('end_time');

            	?>






			<div class="i001-list">
				<div class="i001-list-item cms-mg-obj">
					<a id="eventid-324380"></a>
				<div class="i001-list-image">
							<a href="#">
								<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">
					</a>
				</div><!--i001-list-image-->

						<div class="i001-list-wrap">
							<h4 style="padding-top: 20px;" ><a href="<?php echo get_post_permalink($post->ID); ?>"><?php echo $post->post_title ?></a></h4>

							<h5 style="color: rgb(0, 0, 0); font-family: arial; padding-bottom: 20px; text-transform: capitalize; font-size: 14px; font-weight: bold;"> <?php echo date('l F j Y', strtotime(get_field('date')));  ?>
									&nbsp;&nbsp;<?php if (strpos($end_time, 'pm') !== false) { echo $start_time." till ".$end_time;}
								  	else{ echo $start_time; }
								  ?>
								 </h5>

								<p><?php
								$content = $post->post_content;
$content = preg_replace("/<img[^>]+\>/i", " ", $content);
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo wp_trim_words( $content, 40, '...' ); ?>
								</br>
				  <?php echo '<a type="button"  href="' . get_post_permalink($post->ID) . '" class="i001-css-button new_v01">Read More</a>'; ?>
							</p>
						</div><!--i001-list-wrap-->
				</div><!--i001-list-item cms-mg-obj-->

<img src="./wp-content/uploads/2017/06/879310.png" class="border-bottom">
</div><!--i001-list-->


</div><!--blog-->
</div><!--vc_col-sm-8-->
					<div class="clearing"></div>
			</div><!--container_inner-->
			<?php } ?>
		</div><!--container-->

<div class="clearing"  style="clear:both;"></div>
<?php get_footer(); ?>
