<?php
/**
 *	Web123 WordPress Theme
 *
 */

// This will enqueue style.css of child theme

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );




function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');

//
// function wpb_widgets_init() {
//
// 	register_sidebar( array(
// 		'name' => __( 'Mail Chimp Footer', 'wpb' ),
// 		'id' => 'mail-chimp-1',
// 		'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'wpb' ),
// 		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
// 		'after_widget' => '</aside>',
// 		'before_title' => '<h3 class="widget-title">',
// 		'after_title' => '</h3>',
// 	) );
//
// 	register_sidebar( array(
// 		'name' =>__( 'Secondary sidebar', 'wpb'),
// 		'id' => 'secondary-sidebar-2',
// 		'description' => __( 'Appears on the static front page template', 'wpb' ),
// 		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
// 		'after_widget' => '</aside>',
// 		'before_title' => '<h3 class="widget-title">',
// 		'after_title' => '</h3>',
// 	) );
// 	}
//
// add_action( 'widgets_init', 'wpb_widgets_init' );
//
// /* NEWS custom post type*/
//
//
// function custom_post_type () {
//
// // Set UI labels for Custom Post Type
// 	$labels = array(
// 		'name'                => _x( 'Events', 'Post Type General Name', 'twentythirteen' ),
// 		'singular_name'       => _x( 'Events', 'Post Type Singular Name', 'twentythirteen' ),
// 		'menu_name'           => __( 'Events', 'twentythirteen' ),
// 		'parent_item_colon'   => __( 'Parent Events', 'twentythirteen' ),
// 		'all_items'           => __( 'All Events', 'twentythirteen' ),
// 		'view_item'           => __( 'View Events', 'twentythirteen' ),
// 		'add_new_item'        => __( 'Add New Events', 'twentythirteen' ),
// 		'add_new'             => __( 'Add Events', 'twentythirteen' ),
// 		'edit_item'           => __( 'Edit Events', 'twentythirteen' ),
// 		'update_item'         => __( 'Update Events', 'twentythirteen' ),
// 		'search_items'        => __( 'Search Events', 'twentythirteen' ),
// 		'not_found'           => __( 'Not Found', 'twentythirteen' ),
// 		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
// 	);
//
// // Set other options for Custom Post Type
//
// 	$args = array(
// 		'label'               => __( 'Events', 'twentythirteen' ),
// 		'description'         => __( 'Event and reviews', 'twentythirteen' ),
// 		'labels'              => $labels,
// 		// Features this CPT supports in Post Editor
// 		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
// 		// You can associate this CPT with a taxonomy or custom taxonomy.
// 		'taxonomies'          => array( 'events' ),
// 		/* A hierarchical CPT is like Pages and can have
// 		* Parent and child items. A non-hierarchical CPT
// 		* is like Posts.
// 		*/
// 		'hierarchical'        => false,
// 		'public'              => true,
// 		'show_ui'             => true,
// 		'show_in_menu'        => true,
// 		'show_in_nav_menus'   => true,
// 		'show_in_admin_bar'   => true,
// 		'menu_position'       => 5,
// 		'can_export'          => true,
// 		'has_archive'         => true,
// 		'exclude_from_search' => false,
// 		'publicly_queryable'  => true,
// 		'capability_type'     => 'page',
// 	);
//
// 	// Registering your Custom Post Type
// 	register_post_type( 'event', $args );
// }
//
// add_action( 'init', 'custom_post_type', 0 );
//
// add_filter( 'template_include', 'include_template_function', 1 );
//
// function include_template_function( $template_path ) {
//     if ( get_post_type() == 'event' ) {
//         if ( is_single() ) {
//             // checks if the file exists in the theme first,
//             // otherwise serve the file from the plugin
//             if ( $theme_file = locate_template( array ( 'single-event.php' ) ) ) {
//                 $template_path = $theme_file;
//             } else {
//                 $template_path = get_theme_file_path() . 'templates/single-event.php';
//             }
//         }
//     }
//     return $template_path;
// }

add_action('in_widget_form', 'spice_get_widget_id');

function spice_get_widget_id($widget_instance)

{
    
    // Check if the widget is already saved or not. 
    
    if ($widget_instance->number=="__i__"){
     
     echo "<p><strong>Widget ID is</strong>: Pls save the widget first!</p>"   ;
    
    
  }  else {

        
       echo "<p><strong>Widget ID is: </strong>" .$widget_instance->id. "</p>";
         
 
    }
}
