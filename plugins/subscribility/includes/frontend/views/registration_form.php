<?php
/**
 * Registration form for users to register as a club member.
 * Template Name: Membership Registration
 *
 * This template can be overridden by copying it to wp-content/themes/yourtheme/wp99234/registration_form.php.`
 *
 */

$break = 1;

$membership_options = get_option( 'wp99234_company_membership_types' );

$current_user = wp_get_current_user();

wp_enqueue_script( 'jquery-payment' );

get_header(); 
?>

<style type="text/css">

    /* clearfix hack class */
    .wp99234-cfix:after {
      content: "";
      display: table;
      clear: both;
    }


    /* Membership option area  */
    .wp99234-membership_option{
        border:1px solid #000;
        float:left;
        width:100%;
        background-color: #F9F9F9;
        padding:20px;
        margin-bottom:4%;
    }

    .wp99234-membership_option h4{
        margin-top:0;
        margin-bottom:0;
        font-size:1.2em;
    }

    .wp99234-membership_option:nth-child(2n+1){
        clear:both;
    }

    .wp99234-membership_option.selected{
        border: 1px solid #CCC;
    }

    .wp99234-membership_option.inactive{
        opacity: 0.6;
    }

    .wp99234-membership_option_details ul{
        margin:0;
        padding:0;
        list-style: none;
    }
    @media screen and (min-width: 700px) {
        
        .wp99234-membership_option:nth-child(2n){
            margin-left:4%;
        }
        .wp99234-membership_option{
            width:48%;
        }
    }
    
    .wp99234_delivery_area {
      display: block;
      width: 100%;
      min-height: 150px;
    }



    /* User details, CC details and delivery sections */
    .wp99234-section.user_details,
    .wp99234-section.cc_details,
    .wp99234-section.delivery_details {
        width: 100%;
        box-sizing: border-box;
    }
    @media screen and (min-width: 700px) {
        
        .wp99234-section.user_details,
        .wp99234-section.cc_details {
            float: left;
            width: 50%;
            padding-right: 20px;
        }
        .wp99234-section.delivery_details {
            float: right;
            width: 50%;
            padding-left: 20px;
        }
    }

    .wp99234-section.user_details label,
    .wp99234-section.cc_details label,
    .wp99234-section.delivery_details label,
    .wp99234-section.user_details input,
    .wp99234-section.cc_details input,
    .wp99234-section.delivery_details input {
        width: 100%;
    }

    #wp99234_use_existing_card{
        width:auto;
    }

    /* Form button */
    #wp99234_member_registration_form p.form-submit {
        text-align: right;
    }
    #wp99234_member_registration_form p.form-submit input {
        width: 260px;
    }
   
   #wp99234_membership_options .restricted {
    font-style: italic;
   }


</style>

<form id="wp99234_member_registration_form" action="" method="POST">
  
    <div class="woocommerce">
        <?php wc_print_notices(); ?>
    </div>

    <div class="wp99234-cfix">

        <div id="wp99234_membership_options" class="wp99234-cfix">

            <?php foreach ( apply_filters( 'wp99234_rego_form_membership_options', $membership_options ) as $membership_option ): ?>

                <?php if( ! in_array( $membership_option->visibility, array( 'public', 'restricted' ) ) ) continue; ?>

                <div class="wp99234-membership_option <?php if(isset($_POST['selected_membership']) && $_POST['selected_membership']==$membership_option->id){echo 'selected';}elseif(isset($_POST['selected_membership'])){echo 'inactive';}?>">

                    <h4 class="wp99234-membership_option_title"><?php echo esc_html( $membership_option->name ); ?></h4>

                    <div class="wp99234-membership_option_details">

                        <?php if( ! empty( $membership_option->description ) ): ?>
                            <p><?php echo $membership_option->description; ?></p>
                            <?php if( ! empty( $membership_option->benefits ) ): ?>
                                <a href="#" class="subs-toggle-member-benefits">Show more</a>
                                <p class="wp99234-membership-benefits"><?php echo $membership_option->benefits; ?></p>
                            <?php endif; ?>    
                        <?php endif; ?>
                    </div> 
                    <?php if( $membership_option->visibility == 'public' ) {  ?>

                        <input type="radio" class="selected_membership_radio" name="selected_membership" value="<?php echo $membership_option->id; ?>" <?php if(isset($_POST['selected_membership']) && $_POST['selected_membership']==$membership_option->id){echo 'checked="checked"';}?> />

                        <button class="wp99234-selected_membership" data-original_text="<?php _e( 'Select', 'wp99234' ); ?>" data-selected_text="<?php _e( 'Selected', 'wp99234' ); ?>" data-membership_option_id="<?php echo $membership_option->id; ?>" ><?php if(isset($_POST['selected_membership']) && $_POST['selected_membership']==$membership_option->id){ _e( 'Selected', 'wp99234' );}else{ _e( 'Select', 'wp99234' );}?></button>

                    <?php } else if ( $membership_option->visibility == 'restricted' ) { ?>
              
                        <div class="restricted">You cannot currently sign up for this membership. Please contact us for further information.</div>

                    <?php } ?>

                </div>

            <?php endforeach; ?>

        </div>

        <div class="wp99234-section user_details">

            <h4 class="wp99234-section_title"><?php _e( 'Your Details', 'wp99234' ); ?></h4>

            <div class="wp99234-section_content">

                <?php 
                  $user_fields = array(
                    'first_name' => array(
                      (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'First Name', 'wp99234' ),
                      'default' => get_user_meta( $current_user->ID, 'first_name', true ),
                      'attributes' => array('required' => true)
                    ),
                    'last_name' => array(
                      (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Last Name', 'wp99234' ),
                      'default' => get_user_meta( $current_user->ID , 'last_name', true ),
                    ),
                    'reg_email' => array(
                      (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Email', 'wp99234' ),
                      'default' => ( $current_user ) ? $current_user->user_email : '' ,
                      'attributes' => array('required' => true)
                    ),
                    'phone' => array(
                      (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Phone Number', 'wp99234' ),
                      'default' => get_user_meta( $current_user->ID , 'phone', true ),
                    ),
                    'mobile' => array(
                      (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Mobile Number', 'wp99234' ),
                      'default' => get_user_meta( $current_user->ID , 'mobile', true ),
                    ),
                    'subs_birthday' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Date of Birth', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID , 'birthday', true ),
                        'attributes' => array('required' => (get_option('wp99234_legal_dob_club') == 'yes'), 'id' => 'subs_birthday')
                    )
                  );
                  foreach( $user_fields as $key => $user_field ){
                    WP99234()->_registration->display_field( $key, $user_field );
                  } 
                ?>
        
            </div>

        </div>

        <div class="wp99234-section delivery_details">

            <h4 class="wp99234-section_title"><?php _e( 'Delivery Details', 'wp99234' ); ?></h4>

            <div class="wp99234-section_content">

                <?php $delivery_fields = array(
                    'company_name' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Company Name (optional)', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID , 'shipping_company', true ),
                    ),
                    'shipping_address_1' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Delivery Address', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID , 'shipping_address_1', true ),
                        'attributes' => array('required' => true)
                    ),
                    'shipping_suburb' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Suburb', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID , 'shipping_city', true ),
                        'attributes' => array('required' => true)
                    ),
                    'shipping_postcode' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Postcode', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID , 'shipping_postcode', true ),
                        'attributes' => array('required' => true)
                    ),
                    'shipping_state' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'State', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID , 'shipping_state', true ),
                        'attributes' => array('required' => true)
                    ),
                    'shipping_country' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Country', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID , 'shipping_country', true ),
                        'attributes' => array('required' => true)
                    ),
                    'shipping_instructions' => array(
                        'type'  => 'textarea',
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Delivery notes and instructions (optional)', 'wp99234' ),
                        'default' => get_user_meta( $current_user->ID, 'delivery_instructions', true )
                    )
                  ); 
                
                  foreach( $delivery_fields as $key => $delivery_field ){
                    WP99234()->_registration->display_field( $key, $delivery_field );
                  }
                
                  if(!is_user_logged_in()){
                    ?>
                    <h4 class="wp99234-section_title"><?php _e( 'Create Password', 'wp99234' ); ?></h4>
                    <?php
                    $pass_fields = array(
                      'user_pass' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Password', 'wp99234' ),
                        'type' => 'password' ,
                        'id' => 'password' ,
                        'default' => '',
                        'attributes' => array('required' => true)
                      ),
                      'conf_pass' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Confirm Password', 'wp99234' ),
                        'type' => 'password' ,
                        'id' => 'confirm_password' ,
                        'default' => '',
                        'attributes' => array('required' => true)
                      )
                    );
                    foreach( $pass_fields as $key => $pass_field ){
                      WP99234()->_registration->display_field( $key, $pass_field );
                    } 
                  }
                ?>
            </div>

        </div>

        <div class="wp99234-section cc_details">

            <h4 class="wp99234-section_title"><?php _e( 'Payment Details', 'wp99234' ); ?></h4>

            <div class="wp99234-section_content">

                <?php

                $cc_fields = array(
                    'cc_name' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Cardholder Name', 'wp99234' ),
                        'default' => '',
                        'attributes' => array('required' => true, 'autocomplete' => 'cc-name')
                    ),
                    'cc_number' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Card Number', 'wp99234' ),
                        'default' =>'',
                        'attributes' => array(
                          'required' => true,
                          'class' => 'woocommerce-Input input-text',
                          'placeholder' => "•••• •••• •••• ••••",
                          'maxlength' => 19,
                          'type'=>"tel",
                          'inputmode'=>"numeric",
                          'autocomplete' => 'cc-number'
                        )
                    ),
                    'cc_exp' => array(
                        (get_option('wp99234_club_use_placeholders') == 'yes' ? 'placeholder' : 'label') => __( 'Card Expiry Date', 'wp99234' ),
                        'default' => '' ,
                        'attributes' => array(
                            'placeholder' => 'MM / YY',
                            'required' => true,
                            'autocomplete' => 'cc-exp'
                        ),
                    )
                );

                $has_cc_details = false;

                if( is_user_logged_in() ){
                    $has_cc_meta = get_user_meta( get_current_user_id(), 'has_subs_cc_data', true );
                    if( $has_cc_meta && $has_cc_meta == true ){
                        $has_cc_details = true;
                    }
                }

                if( $has_cc_details ){
                    echo '<div><input type="checkbox" id="use_existing_card" name="use_existing_card" checked="checked" value="yes" style="width:auto;display:inline-block;"/> ' . sprintf( '<label for="use_existing_card" style="width:auto;display:inline-block;vertical-align:middle;margin-bottom:0;margin-left:5px;">Use existing card (%s)</label></div>', get_user_meta( get_current_user_id(), 'cc_number', true ) );
                    echo '<div id="hidden_cc_form"><br /><p>' . __( 'Card details entered here will be stored securely for future use.', 'wp99234' ) . '</p>';
                }

                foreach( $cc_fields as $key => $cc_field ){
                    WP99234()->_registration->display_field( $key, $cc_field );
                }

                if( $has_cc_details ){
                    echo '</div>';
                }

                ?>

            </div>

        </div>
      
        <?php
          do_action('wp99234_preferences_form');
        ?>

    </div>

    <p class="form-submit form-row">
      <label id='message'></label>
      <input type="hidden" name="<?php echo WP99234()->_registration->nonce_name; ?>" value="<?php echo wp_create_nonce( WP99234()->_registration->nonce_action ); ?>" />
      <input type="submit" name="<?php echo WP99234()->_registration->submit_name; ?>" value="<?php _e( 'Sign Up Now', 'wp99234' ); ?>" id="wp99234_member_submit">
    </p>

</form>
<?php 
$limit = (int)(get_option('wp99234_legal_drinking_age'));
$year = date('Y');
echo '<script>jQuery( document ).ready( function($){$("#subs_birthday").datepicker({maxDate:"-'.$limit.'y",changeYear:true,changeMonth:true,minDate:"-105y",yearRange:"'.($year-105).':'.($year-$limit).'"});});</script>';
?>
