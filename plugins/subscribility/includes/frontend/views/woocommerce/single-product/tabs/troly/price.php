<?php
/**
 * Troly Tab Template for showing product description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/troly/awards.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer)
 * will need to copy the new files to your theme to maintain compatibility. We try to do this
 * as little as possible, but it does happen. When this occurs the version of the template file will 
 * be bumped and the readme will list any important changes.
 *
 * @see     https://troly.kayako.com/article/147-woocommerce-templates
 * @author  Troly
 * @version 3.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
# Do NOT use global $product as we will pass you a $product var automatically
# You can only use 'title' and 'content' in this file
?>
<div class="wp99234_meta_item <?php esc_attr_e( $product->title ); ?>">

		<h4 class="wp99234_meta_title"><?php esc_html_e( $product->title ); ?></h4>
		
		
		<!-- Enable this to show your single-unit pricing -->
		<!--
			<p class="price" id="wp99234_product_title">
				<strong>Single Unit: </strong><?php echo $product->content['Single']; ?>
			</p>
		-->
		
		
		<!-- Our Six Pack pricing -->
		<?php if(isset($product->content['6 Pack'])){ ?>
			<p class="price" id="wp99234_product_six_pack">
				<strong>Six Pack: </strong><?php echo $product->content['6 Pack']; ?> / bottle
			</p>
		<?php } ?>
		
		
			<!-- 12 Pack pricing -->
		<?php if(isset($product->content['12 Pack'])){ ?>
			<p class="price" id="wp99234_product_twelve_pack">
				<strong>Case: </strong><?php echo $product->content['12 Pack']; ?> / bottle
			</p>
		<?php } ?>
		
		
		<!--
			If you are wanting to showcase to customers the price they could
			pay as a member, you can show the cheapest per-bottle rate
		-->
		<?php if(isset($product->content['Cheapest Member Price'])) { ?>
			<p class='price' id='wp99234_product_member_prices'>
				<strong>Member Price: </strong><?php echo $product->content['Cheapest Member Price']; ?>
			</p>
		<?php } ?>
		
		<!--
			If you want to showcase the pricing per-unit for all your non-private clubs, uncomment the following snippet
		-->
		<?php
			/*
			if(isset($product->content['Member Prices'])) {
				foreach($product->content['Member Prices'] as $club => $price) {
					echo "
						<p class='price' id='wp99234_product_member_{$club}'>
							<strong>{$club}: </strong> {$price}
						</p>
					";
				}
			}
			*/
		?>
		
		<!-- Do not remove. This assists search engines when indexing your site -->
		<meta itemprop="price" content="<?php echo $product->content['meta']['Single'] ?>" />
		<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->content['meta']['inStock'] ? 'InStock' : 'OutOfStock'; ?>">

</div>