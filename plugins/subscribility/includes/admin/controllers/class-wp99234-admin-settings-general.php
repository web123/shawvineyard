<?php
/**
 * Troly WP99234 General Settings.
 *
 * @author      WP99234
 * @category    Admin
 * @package     WP99234/Admin
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WP99234_Settings_General' ) ) :

/**
 * WP99234_Settings_General defines the general configurations
 */
class WP99234_Settings_General extends WP99234_Settings_Page {

	/**
	 * Constructor.
	 */
	public function __construct() {

		$this->id    = 'general';
		$this->label = __( 'Options', 'wp99234' );

		add_filter( 'wp99234_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
		add_action( 'wp99234_settings_' . $this->id, array( $this, 'output' ) );
		add_action( 'wp99234_settings_save_' . $this->id, array( $this, 'save' ) );
	}

	/**
	 * Get settings array.
	 *
	 * @return array
	 */
	public function get_settings() {

		$settings = apply_filters( 'wp99234_general_settings', array(
			
			array( 'title' => __( 'Alcohol Disclaimer', 'wp99234' ), 'type' => 'title', 'desc' => __( 'The following options allow you to customise how the Troly plugin will interact with your website.', 'wp99234' ), 'id' => 'miscellaneous_options' ),
			array(
				'title'    => __( 'Display a legal drinking age disclaimer', 'wp99234' ),
				'desc'     => __( 'Choose to and how to display a legal drinking age disclaimer on your website.', 'wp99234' ),
				'id'       => 'wp99234_display_legal_drinking_disclaimer',
				'css'      => 'min-width:550px;',
				'default'  => 'overlay',
				'type'     => 'select',
				'desc_tip' => true,
				'options'  => array(
					'no' => __("Don't display a disclaimer", 'wp99234'),
					'overlay' => __('Website overlay when first visiting the site (default)', 'wp99234'),
					'checkout' => __('Notice on the checkout page', 'wp99234')
				)
			),
			array(
				'title'    => __( 'Disclaimer Title', 'wp99234' ),
				'desc'     => __( 'Use this box to set the title to display for the legal drinking age disclaimer.', 'wp99234' ),
				'id'       => 'wp99234_legal_disclaimer_title',
				'css'      => 'min-width:550px;',
				'default'  => 'Welcome!',
				'type'     => 'text',
				'desc_tip' =>  true,
			),
			array(
				'title'    => __( 'Disclaimer Text', 'wp99234' ),
				'desc'     => __( 'Use this box to set the message to display for the legal drinking age disclaimer above.', 'wp99234' ),
				'id'       => 'wp99234_legal_disclaimer_text',
				'css'      => 'min-width:550px; min-height:80px;',
				'default'  => 'By law, we may only supply alcohol to persons aged '.get_option('wp99234_legal_drinking_age').' years or over. We will retain your date of birth for our records.',
				'type'     => 'textarea',
				'desc_tip' =>  true,
			),
			array( 'type' => 'sectionend', 'id' => 'miscellaneous_options'),
			
			array( 'title' => __( 'Store Checkout', 'wp99234' ), 'type' => 'title', 'desc' => __( 'Determine if and how WooCommerce will collect information to ensure your store is only used by persons who are eligible to purchase alcohol.', 'wp99234' ), 'id' => 'capture_option_title' ),
			array(
				'title'    => __( ' Date of Birth', 'wp99234' ),
				'desc'     => __( 'Require customers to enter their date of birth to verify age on checkout.', 'wp99234' ),
				'id'       => 'wp99234_legal_require_dob',
				'css'      => 'min-width:550px;',
				'type'     => 'checkbox',
				'desc_tip' => false
			),
			
			array( 'type' => 'sectionend', 'id' => 'capture_option_title'),
			array( 'title' => __( 'Membership Registration', 'wp99234' ), 'type' => 'title', 'desc' => __( 'When customers sign up on your website, what conditions will be required?', 'wp99234' ), 'id' => 'registration_title' ),
			array(
				'title'    => __( 'Date of Birth', 'wp99234' ),
				'desc'     => __( 'Require new members to provide a date of birth on club sign up.', 'wp99234' ),
				'id'       => 'wp99234_legal_dob_club',
				'css'      => 'min-width:550px;',
				'default'  => true,
				'type'     => 'checkbox',
				'desc_tip' =>  false,
			),
			array(
				'title'    => __( 'Minimum Age', 'wp99234' ),
				'desc'     => __( 'When requiring a birthday on checkout or club sign up, what is the minimum age requirement?', 'wp99234' ),
				'id'       => 'wp99234_legal_drinking_age',
				'css'      => 'min-width:550px; min-height:80px;',
				'default'  => '18',
				'type'     => 'select',
				'options'  => array(
					'18' => __("18 Years", 'wp99234'),
					'21' => __('21 Years', 'wp99234'),
				),
				'desc_tip' =>  true,
			),
			array(
				'title'    => __( 'Under Age Error', 'wp99234' ),
				'desc'     => __( 'When a purchase is made or club sign up is made, what message should be shown to the customer if they are underage?', 'wp99234' ),
				'id'       => 'wp99234_legal_age_error_text',
				'css'      => 'min-width:550px; min-height:80px;',
				'default'  => 'You must be at least 18 years of age purchase alcohol or be a club member from this site.',
				'placeholder'  => 'You must be at least 18 years of age purchase alcohol or be a club member from this site.',
				'type'     => 'textarea',
				'desc_tip' =>  true,
			),
			array(
				'title'    => __( 'Use Placeholders', 'wp99234' ),
				'desc'     => __( 'For input fields, use placeholders instead of labels to indicate field functionality', 'wp99234' ),
				'id'       => 'wp99234_club_use_placeholders',
				'css'      => 'min-width:550px;',
				'default'  => true,
				'type'     => 'checkbox',
				'desc_tip' =>  false,
			),
      
			array( 'type' => 'sectionend', 'id' => 'registration_title'),
			array( 'title' => __( 'Newsletter Sign-up', 'wp99234' ), 'type' => 'title', 'desc' => __( 'Customise how the newsletter sign up will appear', 'wp99234' ), 'id' => 'newsletter_title' ),
			array(
				'title'    => __( 'Use Placeholders', 'wp99234' ),
				'desc'     => __( 'For input fields, use placeholders instead of labels to indicate field functionality', 'wp99234' ),
				'id'       => 'wp99234_newsletter_use_placeholders',
				'css'      => 'min-width:550px;',
				'default'  => true,
				'type'     => 'checkbox',
				'desc_tip' =>  false,
			),
			
			array( 'type' => 'sectionend', 'id' => 'newsletter_title'),

			array( 'title' => __( 'Product Display', 'wp99234' ), 'type' => 'title', 'desc' => __( 'The following options allow you to choose between using Troly product information, or setting product information in WooCommerce.', 'wp99234' ), 'id' => 'product_display_options' ),


			array(
				'title'    => __( 'Use WooCommerce Images', 'wp99234' ),
				'desc'     => __( 'Checking this box will mean WooCommerce will use images found in the Media Gallery<br />and ignore the image sent from Troly.', 'wp99234' ),
				'id'       => 'wp99234_use_wc_product_images',
				'css'      => 'min-width:550px;',
				'default'  => '',
				'type'     => 'checkbox',
				'desc_tip' =>  true,
			),
      
			array( 'type' => 'sectionend', 'id' => 'product_display_options'),
      
			array( 'title' => __( 'Data Synchronisation', 'wp99234' ), 'type' => 'title', 'desc' => __( 'The following options control the flow of information between your Website and Troly.', 'wp99234' ), 'id' => 'sync_options' ),


			array(
				'title'    => __( 'Products', 'wp99234' ),
				'desc'     => __( 'Select which way your products are syncing between Troly and Woocommerce.', 'wp99234' ),
				'id'       => 'wp99234_product_sync',
				'css'      => 'min-width:550px;',
				'default'  => '',
				'type'     => 'select',
				'desc_tip' =>  true,
				'options'  => array(
					'both'=>__('Send AND receive products (Troly ↔ Website)','wp99234'),
					'push'=>__('Only send products (Troly ← Website)','wp99234'),
					'pull' =>__('Only receive products (Troly → Website)','wp99234'),)
			),
			
			array(
				'title'    => __( 'Customers', 'wp99234' ),
				'desc'     => __( 'Select which way your customers are syncing between Troly and Woocommerce.', 'wp99234' ),
				'id'       => 'wp99234_customer_sync',
				'css'      => 'min-width:550px;',
				'default'  => '',
				'type'     => 'select',
				'desc_tip' =>  true,
				'options'  => array(
					'both'=>__('Send AND receive customers (Troly ↔ Website)','wp99234'),
					'push'=>__('Only send customers (Troly ← Website)','wp99234'),
					'pull' =>__('Only receive customers (Troly → Website)','wp99234'),)
			),
			/* TODO: we need a setting to associate club members to a different wp user type */

			array(
				'title'    => __( 'Clubs', 'wp99234' ),
				'desc'     => __( 'Shows how your clubs will be synced between Troly and Woocommerce', 'wp99234' ),
				'id'       => 'wp99234_club_sync',
				'css'      => 'min-width:550px;',
				'default'  => '',
				'type'     => 'select',
				'desc_tip' =>  true,
				'options'  => array(
					'both'=>__('Send AND receive clubs (Troly ↔ Website)','wp99234'),
					// 'push'=>__('Only send clubs (Troly ← Website)','wp99234'),
					// 'pull' =>__('Only receive clubs (Troly → Website)','wp99234')
					)

			),

			array( 'type' => 'sectionend', 'id' => 'sync_options'),

			array( 'title' => __( 'Reporting', 'wp99234' ), 'type' => 'title', 'desc' => __( 'Define the type and also amount of information being displayed in the runtime console.', 'wp99234' ), 'id' => 'log_options' ),

			array(
				'title'    => __( 'Transaction tracking', 'wp99234' ),
				'desc'     => __( 'Select what should appear in the logs. Used only for troubleshooting.', 'wp99234' ),
				'id'       => 'wp99234_reporting_sync',
				'css'      => 'min-width:550px;',
				'default'  => '',
				'type'     => 'select',
				'desc_tip' =>  true,
				'options'  => array(
					'minimum'=>__('Orders and payment only','wp99234'),
					'medium' =>__('Products, clubs and customers','wp99234'),
					'verbose'=>__('Full details','wp99234'),)
			),

			array( 'type' => 'sectionend', 'id' => 'log_options'),

		) );

		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings );
	}

	/**
	 * Save settings.
	 */
	public function save() {
		
		$settings = $this->get_settings();

		WP99234_Admin_Settings::save_fields( $settings );
	}

}

endif;

return new WP99234_Settings_General();
