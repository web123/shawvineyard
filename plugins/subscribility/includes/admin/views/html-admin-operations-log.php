<div class="wrap">
	<h2>Troly Event Log</h2>
	<p>To view detailed log files, including debug information, open <a href="../wp-content/plugins/subscribility/logs/" target="_blank"><strong>logs</strong></a> directory in the Troly plugin.</p>
	<p><a href="../wp-content/plugins/subscribility/logs/<?php echo strftime("%F");?>.txt" target="_blank"><strong>View Latest Technical Log</strong></a>
	<p>If you are setting multiple errors here, <a href="https://help.troly.io" target="_blank">visit the Troly Help Centre</a>.</p>
	
	<p>Reset Troly log file by clicking <a href="admin.php?page=wp99234-operations&tab=log&wp99234_reset_log=1">here</a>.</p>
	
	<h3>Events</h3>
	<?php
	
		$csv_log_file = content_url() . '/subs_log.csv';
		
		if (($csv_log = fopen($csv_log_file, 'r')) !== FALSE) {
			
			echo "
			<table class='wp99234_log_table'>
				<thead>
					<th>Time (UTC)</th>
					<th>Type</th>
					<th>What</th>
					<th>Details</th>
				</thead>
				<tbody>
			";
			
			while (($data = fgetcsv($csv_log)) !== FALSE) {
				echo "<tr>";
				foreach ($data as $fields) {
					echo "<td>".$fields."</td>";
				}
				echo "</tr>";
			}
			
			echo "</tbody></table>";
		}
	
	?>

</div>