=== Troly for Wordpress ===
Contributors: subscribility
Tags: troly,woocommerce,wine,wine clubs,craft beers
Requires at least: 4.8.0
Tested up to: 4.9.1
Stable Tag: 2.6.1
PHP version: 5.6 and above
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Let your customers order wine from your website. Keep your products, customers, clubs and orders in sync between your website and Troly. 

## Description

This plugin lets you sell your wines right from your website. You will be able to import all your products from your Troly account in just a click. 
Process online payments, sign up new customers to your clubs and manage everything from one place. 

Key features:

- Keep all your products and customers in sync between Wordpress and Troly.
- Capture sales directly on your website and process them in Troly
- Process payments using the payment gateway configured in Troly
- Sign up new members to your wine club
- Bulk product import / export
- Bulk customers and members import / export
- Import memberships / clubs

## Installation
**You must install WooCommerce before installing Troly for Wordpress.**

1. Enable the Wordpress add-on in your Troly account (see screenshot)
2. Download and enable the plugin (or download and upload manually to your plugins folder)
3. Link your website to your Troly account (see [screenshot](https://wordpress.org/plugins/subscribility/screenshots/))

If you want to display a form that lets visitors signup to your clubs, you need to add the shortcode `[wp99234_registration_form]` to a page. 

Once that is done, you can run an initial import of all your customers, products and memberships in your Troly account. You can also export your customers and products **from** your website **to** Troly. 

## Frequently Asked Questions
### What do I need to use the plugin? 
The plugin is only useful if you have a Troly account to manage your operations. [Join us today!](https://troly.io/apply/)

### How can customers pay? 
* Using your configured online payment gateway in Troly. 
* Using the a payment gateway provided by WooCommerce. This will mark and an order as paid in Troly.
	* Refunding an order must happen in Troly and will mark it as in-progress in Wordpress. You must see to the refunding of monies taken through the payment gateway separately.

### How are shipping prices calculated? 
When a customer places an order, the shipping fee will be calculated based on the size of their order and the delivery address. Those prices are calculated based on the settings you have entered in your Troly account. 

### Do I need an SSL certificate (https)?
SSL certificates improve the security of your website and are highly recommended; if you do not use SSL, the plugin will still function as normal.

### Is Guest Checkout available? 
Yes. To enable it, open WooCommerce > Settings > Checkout and check the _Enable guest checkout_ checkbox and save your changes.

### Where do product images come from? 
By default, any image you upload for a product in Troly will be used to display the product on your website.

If you have a higher resolution or special web version of your product image, you can upload directly into WooCommerce and override the image from Troly.

**All images** must come from Troly _**OR**_ from WooCommerce. It is not possible for some images to come from Troly, while others are uploaded via WooCommerce. 
To select where images are coming from, go to the Settings page of the plugin, and check or uncheck the "Use WooCommerce product images" checkbox. 

== Screenshots ==
1. This screenshot shows how to install the Wordpress add-on in Troly

## Changelog
###Version 2.6.1
- Introduced the ability to use templates for product tabs
	- Troly will now allow you to customise your product display inside the product tabs

###Version 2.6.0
- Improved the logging system
	- Errors will now be communicated when importing or exporting clubs, customers and products
	- Log files will now contain more information to help your devs debug
	- Improved styling of the log table when importing
	- Added seconds to the subs_log.csv file for increased granularity of events
- Improved behind-the-scenes functionality of the operations page
- Fixed an issue where the Troly shipping method may disappear at checkout
- Added additional template directories to allow theme overrides
	- /wp99234/ and /troly/ will now be searched from within your theme directory
- Added Troly club pricing to product display within the Wordpress admin area
	- This will allow for the displaying of current membership prices in WooCommerce
	- Member prices must - and can only - be changed in Troly
- Improved exception handling when talking to Troly
	- HTTP-level exceptions are now logged to the daily log files
- Removed PHP notices and warning from a few different places

###Version 2.5.0
- Resolved an issue on club sign up where the Date of Birth setting may be incorrectly enforced
- Improved PHP requirements checking
- Improved shipping cost logic when using local pickups
- Fixed club sign up form selection failing
- Fixed 'View in Troly' link inside WooCommerce order view
- Re-added notices to the administration panel
- Added the option for newsletter or club sign up fields to use labels or placeholders for field names

###Version 2.4.7
- Resolved products appearing when _Visible Online_ is disabled in Troly
- Resolved _Out of Stock_ not applying on some products

###Versions 2.4.1 - 2.4.6
- Resolved various issues with Date of Birth on checkout and club sign up pages
- Resolved an issue that called HTTP content over HTTPS

###Version 2.4.0
- Use the *local pickup* method for pickups. Paid pickups are not currently supported
- Allow date of birth to be captured on the wine club sign up form

###Version 2.3.3
- Improved the processing of payments into Troly
- Improve customer syncronisation with Troly

###Version 2.3.2
- Fixed an error appearing on some pages for a 'PLUGIN_VERSION' constant

###Version 2.3.1
- Fixed Wordpress and Troly connection reporting 'process halted'
- Fixed shipping rates not applying under some circumstances

###Version 2.3.0
- ***New:*** Add the ability to require _and_ capture date of birth on checkout page
- Resolved an issue with tab views showing an error message
- Resolved an issue where the payment gateway may not save settings
- Improved descriptions of Troly fields added in the _Users_ section of Wordpress
- Improved compatibility with latest WooCommerce version

###Version 2.2.0
- Fixed product import failing at random intervals
- Increased the batch size when importing for customer records

###Version 2.1.9
- Added exporting functionality for PayPal

###Version 2.1.8
- Fixed an issue where some products may not add to cart

###Version 2.1.7
- Improved import and export messages when pulling or pushing products

###Version 2.1.6
- Resolve an issue with membership prices not appearing
- Improved performance for importing products from Troly

###Version 2.1.5
- Rebranded the plugin to be under the [Troly](https://troly.io) name

###Version 2.1.4
- Added a filter ('wp99234_rego_form_membership_options') for reordering club memberships on the sign up/registration page.

###Version 2.1.3
- Added support for WooCommerce guest checkout

###Version 2.1.2
- Added support for WooCommerce shipping zones
- All notifications are now enabled by default for all new members

###Version 2.1.0
- IMPORTANT UPDATE: fixes an issue with the product quantities sent to Troly when an order is placed
- Fixes a redirection issue on the membership signup form

###Version 2.0
Our biggest overhaul of the plugin to date. 

- Improved interface for a better user experience
- Choose between using images uploaded in Troly, or directly in WooCommerce
- Override the default signup form template by copying into a folder called wp99234 in your child theme's root folder
- Only one phone number is necessary to sign up to your clubs, instead of two
- Translate product variations into a number of bottles (6 packs, 12 packs etc...)
- Several bugfixes and smaller improvements

###Version 1.2.31

- First official release

## Upgrade notice
### 2.0
Version 2 of the plugin uses version 1.1 of the Wordpress add-on in Troly. Support for version 1.0 of the Wordpress add-on is now deprecated. 


### 1.2.31 ###
This version makes updating the plugin much easier. It also fixes a number of issues with the member signup process. 

## Dependencies
+ WooCommerce Version 3.1.0 (tested up to 3.1.2)
* An active [Troly](https://troly.io/) account
